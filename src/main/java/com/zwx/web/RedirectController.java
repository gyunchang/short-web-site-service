package com.zwx.web;

import com.zwx.service.RedirectService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;

/**
 * @Title 短网址重定向服务
 */
@Controller
@RequestMapping("/")
public class RedirectController {
    @Autowired
    private RedirectService redirectService;

    /**
     * 短网址服务首页
     *
     * @return
     */
    @GetMapping("/")
    public String index() {
        return "index";
    }

    /**
     * 短网址重定向
     *
     * @param key
     * @return
     */
    @GetMapping("/{key}")
    public String redirectIndex(@PathVariable String key) {
        return redirectService.redirectIndex(key);
    }
}
